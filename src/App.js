import React, { useState } from 'react' 
import { OrbitControls } from "drei"
import { Canvas, useFrame } from 'react-three-fiber'
import Game from './components/Game'
import PostProcessing from './components/PostProcessing'
import Lighting from './components/Illumination'
import Status from './components/Status'
import logicWinner from "./helpers/logicWinner"
import Scenario from './components/Scenario'
import Grid from './components/Grid'

const App = () => {
    const [board, setBoard] = useState(Array(9).fill(null))
    const [nextPlayer, setNextPlayer] = useState(true)
    const winner = logicWinner(board)

    const handleClick = i => {
        const cloneBoard = [...board]
        if ( winner || cloneBoard[i] ) return
        cloneBoard[i] = nextPlayer ? 'Ryu' : "Ken"
        setBoard(cloneBoard)
        setNextPlayer(!nextPlayer)
    }

    const renderMoves = () => {
        setBoard(Array(9).fill(null))
    }

    return(
        <>
            <Canvas camera={
              { 
                fov: 70,
                position: [3.5, -1.7, -10]}
              }>
                <ambientLight />
                <ambientLight intensity={0.5} />
                <spotLight position={[10, 10, 10]} angle={0.15} penumbra={1} />
                <pointLight position={[-10, -10, -10]} />
                <OrbitControls />
                <Lighting />
                <Scenario />
                <Grid />
                <Game 
                  board={board}
                  nextPlayer={nextPlayer} 
                  winner={winner} 
                  handleClick={handleClick}
                />
                <PostProcessing />
            </Canvas>
            <Status 
              board={board} 
              nextPlayer={nextPlayer} 
              winner={winner} 
              renderMoves={renderMoves}
            />
        </>
    )
}

export default App