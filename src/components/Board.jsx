import React from 'react'
import Box from './Box'

const cells = [
    [-1.7, 3, 0],
    [1.2, 3, 0],
    [4, 3, 0],
    [-1.7, 0, 0],
    [1.2, 0, 0],
    [4, 0, 0],
    [-1.7, -2.4, 0],
    [1.2, -2.4, 0],
    [4, -2.4, 0]
]

const Board = ({squares, onClick}) => {
  return( 
        <>
            {cells.map((box, idx) => (
                <Box 
                    key={idx}
                    shape={squares[idx]} 
                    position={box} 
                    onClick={() => onClick(idx)} 
                />
            ))}
        </>
    )
}

export default Board
