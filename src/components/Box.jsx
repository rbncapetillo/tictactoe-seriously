import React from 'react'
import { DoubleSide } from "three"
import Ryu from './Ryu'
import Ken from './Ken'

const Box = ({ shape, position, onClick }) => {
  let landingPlayer;
  shape === "Ryu" ? landingPlayer = <Ryu 
                            position={position} 
                          />
  : shape === "Ken" ? landingPlayer = <Ken 
                              position={position} 
                            />
  : landingPlayer = null

  return (
    <>
      {landingPlayer}
      <mesh position={position} onClick={onClick}>
        <boxBufferGeometry 
          attach="geometry" 
          args={[2.4, 2.4, 2.4 ]}
        />
        <meshLambertMaterial
          transparent
          depthWrite={false}
          attach="material"
          opacity={0}
          color={"blue"}
          alphaTest={0}
          side={DoubleSide} 
        />
      </mesh>
    </>
  )
}

export default React.memo(Box)