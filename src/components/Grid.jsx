import React from 'react'
import Tictactoe from './Tictactoe'

const Grid = () => {
  return( 
    <>
        <Tictactoe 
            position={[2.4, 0, 0]} 
        />
        <Tictactoe 
          position={[0, 0, 0]} 
        />
        <Tictactoe 
          position={[1.2, -1.2, 0]} 
          rotation={[0, 0, Math.PI/2]}
        />
        <Tictactoe 
          position={[1.2, 1.2, 0]} 
          rotation={[0, 0, Math.PI/2]}
        />
    </>
    )
}

export default React.memo(Grid)
