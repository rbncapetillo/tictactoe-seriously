import React from 'react'

const Illumination = () => {
    return (
      <>
        <ambientLight />
        <spotLight
          castShadow
          intensity={0.2}
          angle={Math.PI / 2}
          position={[70, 70, 70]}
          penumbra={1}
          shadow-mapSize-width={1024}
          shadow-mapSize-height={1024} />
        <pointLight 
          position={[ -70, 100, -150 ]} 
          intensity={0.5} 
        />
      </>
    )
  }

  export default React.memo(Illumination)