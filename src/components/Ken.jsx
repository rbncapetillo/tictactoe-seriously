import React, { useRef, useState } from 'react'

const Ken = ({ position }) => {
  const mesh = useRef()
  const [hovered, setHover] = useState(false)
  const [active, setActive] = useState(false)
  const color = "red"
  return (
    <mesh 
      position={position}
      ref={mesh}
      castShadow
      receiveShadow
      scale={active ? [1.3, 1.3, 1.3] : [1, 1, 1]}
      onClick={(e) => setActive(!active)}
      onPointerOver={(e) => setHover(true)}
      onPointerOut={(e) => setHover(false)}
    >
      <boxBufferGeometry args={[1, 1, 1]} />
      <meshStandardMaterial 
        attach="material" 
        color={hovered ? '#950abb' : color } 
        metalness={0.9}
        roughness={0.1}
        />
      <sphereBufferGeometry
        attach="geometry" 
        args={[0.80, 20, 20]}
      />
      <sphereBufferGeometry 
        attach="geometry" 
        args={[0.80, 20, 20]}
      />
    </mesh>
  )
}

export default React.memo(Ken)