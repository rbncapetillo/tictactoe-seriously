import React, { useState, memo } from 'react'
import styled, { css, createGlobalStyle } from 'styled-components'
import sfken from "../assets/music/SfKen.mp3"

const audio = new Audio(sfken)

const Status = ({board, nextPlayer, winner, renderMoves}) => {
  const [sound, setSound] = useState(true)

  let status
  winner ? status = `¡¡¡ The winner is ${winner} !!!`
  : !winner && !board.includes(null) ? status = "Tie!"
  : status = "Next player: " + (nextPlayer ? "Ryu" : "Ken")

  const trigger = () =>{
    if (sound){
      audio.play()
      audio.loop = true
    }
    else{
      audio.pause()
    }
    setSound(!sound)
  }

  return (
    <>
      <TitleGame>
        <h1>Tic Tac Toe</h1>
      </TitleGame>
      <ConfigSound 
        onClick={trigger}
      >
        <p>{sound ? <s>Unmute</s> : "Mute"}</p>
      </ConfigSound>
      <WinnerIndicator>
        <p>{status}</p>
      </WinnerIndicator>
      <ResetButton 
        onClick={renderMoves}
      >
        <p>{ board.every(x => x === null) ? "" : "Restart" } </p>
      </ResetButton>
      <GlobalStyles />
    </>
  )
}

const base = css`
  font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
  position: absolute;
  text-transform: uppercase;
  font-weight: 300;
  line-height: 0.5em;
  pointer-events: none;
  color: rgba(109, 45, 134, 0.76);
`

const TitleGame = memo(styled.div`
  ${base}
  top: -15px;
  left: 680px;
  text-decoration: underline;
  font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
  text-transform: capitalize;
  font-size: 40px;
  pointer-events: all;
  cursor: pointer;
  @media only screen and (min-width: 600px) and (max-width: 1199px) {
    left: 180px;
  }
  @media only screen and (min-width: 1200px) {
    left: 100px;
  }
`)

const ConfigSound = styled.div`
  ${base}
  text-align: right;
  top: 10px;
  right: 20px;
  pointer-events: all;
  font-size: 36px;
  font-weight: 900;
  cursor: pointer;
`

const WinnerIndicator = styled.div`
  ${base}
  bottom: 30px;
  left: 30px;
  width: 800px;
  font-size: 36px;
  font-weight: 900;
`

const ResetButton = styled.div`
  ${base}
  bottom: 20px;
  right: 20px;
  width: 200px;
  font-size: 36px;
  font-weight: 900;
  text-align: right;
  pointer-events: all;
  color: rgba(147, 72, 177, 0.76);
  cursor: pointer;
`

const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  html,
  body,
  #root {
    width: 100%;
    height: 100vh;
    margin: 0;
    padding: 0;
    overflow: auto;
    padding: 0px;
  }
  body {
    position: fixed;
    overflow: hidden;
    overscroll-behavior-y: none;
    font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
  }
`
export default React.memo(Status);