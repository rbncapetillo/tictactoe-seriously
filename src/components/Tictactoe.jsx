import React from 'react'

const Tictactoe = (props) => {
  return (
    <mesh {...props}>
      <boxBufferGeometry 
        attach="geometry" 
        args={[0.20, 10, 0.50]} 
      />
      <meshStandardMaterial 
        attach="material" 
        metalness={1} 
        roughness={0.1} 
        color={0x0000FF} 
      />
    </mesh>
  )
}

export default Tictactoe